# AWS region where you want to create the EC2 instance
$region = "us-east-1"

# EC2 instance details
$instanceType = "t2.micro"
$amiId = "ami-XXXXXXXXXXXXXXXXX"  # Replace with your desired AMI ID
$keyName = "your-key-pair-name"    # Replace with your key pair name
$securityGroupId = "sg-XXXXXXXX"  # Replace with your security group ID
$subnetId = "subnet-XXXXXXXX"      # Replace with your subnet ID

# Create a new EC2 instance
$instance = New-EC2Instance -ImageId $amiId -InstanceType $instanceType -KeyName $keyName -SecurityGroup $securityGroupId -SubnetId $subnetId -Region $region

# Get the public IP address of the instance
$publicIpAddress = $instance.Instances[0].PublicIpAddress

Write-Host "EC2 instance $($instance.Instances[0].InstanceId) is being created..."

# Wait until the instance is running
Wait-EC2Instance -InstanceId $instance.Instances[0].InstanceId -Status 'InstanceRunning' -Region $region

Write-Host "EC2 instance $($instance.Instances[0].InstanceId) is now running with public IP: $publicIpAddress"
