#!/bin/bash

# AWS region where you want to create the EC2 instance
REGION="us-east-1"

# EC2 instance details
INSTANCE_TYPE="t2.micro"
AMI_ID="ami-XXXXXXXXXXXXXXXXX"  # Replace with your desired AMI ID
KEY_PAIR_NAME="your-key-pair-name"
SECURITY_GROUP_IDS="sg-XXXXXXXXXXXXXXXXX"  # Replace with your security group IDs
SUBNET_ID="subnet-XXXXXXXXXXXXXXXXX"  # Replace with your subnet ID
INSTANCE_NAME="MyEC2Instance"

# Launch the EC2 instance
INSTANCE_ID=$(aws ec2 run-instances \
  --region $REGION \
  --image-id $AMI_ID \
  --instance-type $INSTANCE_TYPE \
  --key-name $KEY_PAIR_NAME \
  --security-group-ids $SECURITY_GROUP_IDS \
  --subnet-id $SUBNET_ID \
  --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$INSTANCE_NAME}]" \
  --output json \
  | jq -r '.Instances[0].InstanceId')

echo "EC2 instance $INSTANCE_ID is being created..."

# Wait until the instance is running
aws ec2 wait instance-running --region $REGION --instance-ids $INSTANCE_ID

# Get the public IP address of the instance
PUBLIC_IP=$(aws ec2 describe-instances \
  --region $REGION \
  --instance-ids $INSTANCE_ID \
  --query "Reservations[0].Instances[0].PublicIpAddress" \
  --output text)

echo "EC2 instance $INSTANCE_ID is now running with public IP: $PUBLIC_IP"